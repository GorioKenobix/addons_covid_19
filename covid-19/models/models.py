# -*- coding: utf-8 -*-
from odoo import models, fields, api

class covid_19(models.Model):
    _name = 'covid.covid.19'

    source = fields.Char(string='Fuente de informacion',required=True)
    date = fields.Datetime(string='Fecha',required=True,default=fields.Datetime.now())
    country_id= fields.Many2one('res.country',required=True)
    infected=fields.Integer(string='Infectados',required=True,defautl=0)
    recovered=fields.Integer(string='Recuperados',required=True,default=0)
    deseaced=fields.Integer(string='Fallecidos',required=True,default=0)
    total_infected=fields.Integer(string='Infectados totales',compute='set_total_infected',required=True,default=0)
    total_recovered=fields.Integer(string='Recuperados totales',compute='set_total_recovered',required=True,default=0)
    total_death=fields.Integer(string='Total de fallecidos',compute='set_total_deseaced',required=True,default=0)
    def set_total_infected(self):

        for data in self:
            domain=[
                    ('country_id','=',data.country_id.id),
                    ('date','<',data.date),

                    ]
            records=self.search(domain)
            Infecteds=records.mapped('infected')
            data.total_infected=sum(Infecteds)+data.infected

    def set_total_recovered(self):

            for data in self:
                domain=[
                        ('country_id','=',data.country_id.id),
                        ('date','<',data.date),

                        ]
            records=self.search(domain)
            Recovered=records.mapped('recovered')
            data.total_recovered=sum(Recovered)+data.recovered

    def set_total_deseaced(self):

            for data in self:
                domain=[
                        ('country_id','=',data.country_id.id),
                        ('date','<',data.date),

                        ]
            records=self.search(domain)
            Muertos=records.mapped('deseaced')
            data.total_death=sum(Muertos)+data.deseaced
